# Macro Imaging Setup <br>
Setup is designed to provide a very stable mounting point for a digital camera.<br>
System allows for focussing via linear stage, with a rotational component to align to the substrate. The substrate can be indexed in the same location relative to the camera each time which will provide a good way of tracking nucleation of a degraded phase as well as tracking of the spread.



## Models <br>
Models are available for custom parts, as well as an Assembly.<br>
Models will be updated if setup changes or is improved.